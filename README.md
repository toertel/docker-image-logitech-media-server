<!--
SPDX-FileCopyrightText: 2014 JingleManSweep
SPDX-FileCopyrightText: 2015-2018 Lars Kellogg-Stedman <lars@oddbit.com>
SPDX-FileCopyrightText: 2019-2025 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/docker-image-logitech-media-server/badges/master/pipeline.svg)](https://gitlab.com/toertel/docker-image-logitech-media-server/pipelines)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/toertel/docker-image-logitech-media-server)](https://api.reuse.software/info/gitlab.com/toertel/docker-image-logitech-media-server)
[![Docker stars](https://img.shields.io/docker/stars/toertel/logitech-media-server)](https://hub.docker.com/r/toertel/logitech-media-server)
[![Docker pulls](https://img.shields.io/docker/pulls/toertel/logitech-media-server)](https://hub.docker.com/r/toertel/logitech-media-server)

# Docker Container for Lyrion Music Server

This Docker image runs [Lyrion Music Server](https://github.com/lms-community/slimserver)
(formerly Logitech Media Server, SlimServer, SqueezeCenter, and Squeezebox Server) on multiple architectures:

- amd64 (x86_64)
- arm32v7 (e.g. Raspberry Pi 2/3/4)
- arm64v8

This project forks the archived
[larsks/docker-image-logitech-media-server](https://github.com/larsks/docker-image-logitech-media-server)
repository. The source code and automatic builds are maintained at
[GitLab toertel/docker-image-logitech-media-server](https://gitlab.com/toertel/docker-image-logitech-media-server).
The container images are available in multiple tagged versions through
[Docker Hub toertel/logitech-media-server](https://hub.docker.com/repository/docker/toertel/logitech-media-server).

| Tag                                      | Example  | Description                                                                            |
| ---------------------------------------- | -------- | -------------------------------------------------------------------------------------- |
| latest                                   | latest   | The *latest* tag always marks the latest released version.                             |
| v\<major\>.\<minor\>.\<patch\>-\<extra\> | v8.2.1-3 | Third release of container image with LMS 8.2.1; exact match of tag in Git repository. |
| \<major\>.\<minor\>.\<patch\>            | 8.2.1    | Latest release of container image with LMS 8.2.1.                                      |
| \<major\>.\<minor\>                      | 8.2      | Latest release of container image with LMS 8.2 series.                                 |
| \<major\>                                | 8        | Latest release of container image with LMS 8 series.                                   |
| next                                     | next     | Not an official release; newest build of master branch in Git repository.              |

| :warning: Unfortunately the last official Squeezebox Radio firmware (7.7.3) comes with a bug which prevents it from connecting correctly to Logitech Media Server 8+. You can install the community firmware which does not have the bug. See https://forums.slimdevices.com/forum/user-forums/3rd-party-software/1699666-how-and-why-to-install-the-community-firmware-step-by-step for details. |
| --- |

## Run directly

```
docker run -d --init \
           -p 9000:9000 \
           -p 9090:9090 \
           -p 3483:3483 \
           -p 3483:3483/udp \
           -v /etc/localtime:/etc/localtime:ro \
           -v <local-state-dir>:/srv/squeezebox \
           -v <audio-dir>:/srv/music \
           toertel/logitech-media-server
```

The web interface runs on port 9000. If you also want this available
on port 80 (so you can use `http://yourserver/` without a port number
as the URL), you can add `-p 80:9000`, but you *must* also include `-p
9000:9000` because the players expect to be able to contact the server
on that port.

In case you are running multiple LMS instances in the same network
which shall be able to discover another you have to add the option
`--network host`. You should only do this if you really need this feature and
you have an idea what the side effects are.

The *local-state-dir* `/srv/squeebox` stores prefs, logs, and cache. If it is
not explicitly mounted during the start of the container an anonymous volume
will be created.

## Using docker-compose

There is a [docker-compose.yml](docker-compose.yml) included in this repository
that you will let you bring up a LMS container using `docker-compose`. The
compose file includes the following:

```
volumes:
  - "${AUDIO_DIR}:/srv/music:ro"
  - "${PLAYLIST_DIR}:/srv/playlists"
```

To provide a value for `AUDIO_DIR` and `PLAYLIST_DIR`, create file named `.env`
that points `AUDIO_DIR` and `PLAYLIST_DIR` at the location of your music
library and the playlist folder respectively, for example:

```
AUDIO_DIR=/home/USERNAME/Music
PLAYLIST_DIR=/home/USERNAME/Playlists
```

The *local-state-dir* is created in a named volume *squeezebox*. If you want to use
a host directory instead you need to modify the [docker-compose.yml](docker-compose.yml)
accordingly.

## Additional parameters

The user and group ID used to run LMS can be configured using the container's
`SQUEEZE_UID` and `SQUEEZE_GID` environment variables.

## Image building process

The official Docker Hub images at [toertel/logitech-media-server](https://hub.docker.com/repository/docker/toertel/logitech-media-server)
are built using GitLab CI/CD. See [.gitlab-ci.yml](https://gitlab.com/toertel/docker-image-logitech-media-server/-/blob/master/.gitlab-ci.yml)
for details.
