# SPDX-FileCopyrightText: 2014 JingleManSweep
# SPDX-FileCopyrightText: 2015-2018 Lars Kellogg-Stedman <lars@oddbit.com>
# SPDX-FileCopyrightText: 2019-2025 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

FROM debian:bookworm-slim

ARG BUILD_DATE
ARG IMAGE_VERSION
ARG COMMIT_SHA
ARG LMS_VERSION=9.0.1
ARG ARG LMS_URL=https://downloads.lms-community.org/LyrionMusicServer_v${LMS_VERSION}/lyrionmusicserver_${LMS_VERSION}_all.deb

# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL \
	org.opencontainers.image.created="${BUILD_DATE}" \
	org.opencontainers.image.authors="Mark Jonas <toertel@gmail.com>" \
	org.opencontainers.image.url="https://gitlab.com/toertel/docker-image-logitech-media-server" \
	org.opencontainers.image.documentation="https://gitlab.com/toertel/docker-image-logitech-media-server/-/blob/${COMMIT_SHA}/README.md" \
	org.opencontainers.image.source="https://gitlab.com/toertel/docker-image-logitech-media-server.git" \
	org.opencontainers.image.version="${IMAGE_VERSION}" \
	org.opencontainers.image.revision="${COMMIT_SHA}" \
	org.opencontainers.image.vendor="Mark Jonas <toertel@gmail.com>" \
	org.opencontainers.image.licenses="" \
	org.opencontainers.image.ref.name="" \
	org.opencontainers.image.title="Docker image for Lyrion Music Server" \
	org.opencontainers.image.description="Docker image for Lyrion Music Server powers Squeezebox audio players to play local music, music from streaming services, and radio stations" \
	maintainer="Mark Jonas <toertel@gmail.com>"

HEALTHCHECK --interval=5m --timeout=3s \
	CMD curl --fail -s http://localhost:9000 || exit 1

ENV \
	SQUEEZE_VOL=/srv/squeezebox \
	LANG=C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get -y --no-install-recommends install \
		curl \
		ca-certificates \
		faad \
		flac \
		lame \
		sox \
		libcrypt-openssl-rsa-perl \
		libio-socket-ssl-perl \
		tzdata \
		&& \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

RUN curl -Lsf -o /tmp/lms.deb ${LMS_URL} && \
	dpkg -i /tmp/lms.deb && \
	rm -f /tmp/lms.deb && \
	apt-get clean

# This will be created by the entrypoint script.
RUN userdel squeezeboxserver

VOLUME $SQUEEZE_VOL
EXPOSE 3483 3483/udp 9000 9090

COPY entrypoint.sh start-squeezebox.sh /
ENTRYPOINT ["/entrypoint.sh"]
